---
category: bioplastics
title: Foam Bioplastic
author: Maddie and Mathilde
difficulty: Moderate
prep-time: 0.5
curing-time: 72
ingredients: Glycerol, Agar, Dishwashing Liquid
equipment: Bio Pot, Teaspoon, Oven, Beaker, Measuring Cup, Silicone Mat, Silicone Frames, 1000g Scales, Hotplate, Fork
photo: recipes/foambioplastic.jpeg
---

## Instructions
### Quantities Required

* Water - 250ml
* Glycerol - 33g
* Agar - 10g
* Dishwashing Soap - 40ml

### Steps

1. Gather all equipment and ingredients
2. Set up the silicone mat and frame to cover an A3 oven tray
3. Measure all ingredients and add them to the bio pot (except for the dishwashing soap)
4. Heat ingredients on hotplate, stirring continuously until boiling and all ingredients are combined
5. Once boiled, add the dishwashing soap and beat with an electric mixture so that it begins to foam
6. Take off the heat and carefully poor the bioplastic mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame
7. Place in oven at 25 degrees for 72 Hours

### Tips

This recipe will create a bioplastic that is both of a foamy and thick plastic consistency. To create a foam bioplastic that is made primarily from the foam, you can separate the bubbles from the liquid mixture (after beating) and spread this onto the silicone tray to create a lighter, foamier bioplastic.

You can also flip the foam bioplastic halfway through its curing time to allow for the extra moisture on the bottom side to evaporate


### Photos

![foambioplastic]({{site.imageurl}}recipes/foambioplastic.jpeg){:.img-fluid}

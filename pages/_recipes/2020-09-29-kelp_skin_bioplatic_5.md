---
category: bioplastics
title: Kelp Skin Bioplastic No. 5 (Rubbery) - From FRESH Kelp
author: Jaqueline Solis
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,Glycerol,White Vinegar,Agar,Tapioca Flour,Cornflour,Alginate,seaweed_skin_from_fresh_kelp
equipment: 1000g Scales,Teaspoon,Measuring Cup,biopot,Silicone Spatula,hand_blender,Oven,Silicone Mat
photo: recipes/kelpskin5_1.jpeg
---




 Quantities Required


 500 ml Water

 20 ml Glycerol

 10 ml Vinegar

 10 gm Agar

 10 gm Tapioca

 10 gm Cornflour

 10 gm Alginate

 120 gm Seaweed Skin from Fresh Kelp




Rehydration:


 8 ml Water

 2 ml Glycerol




 Steps


 For this recipe you will need the skin you scrapped off from the Kelp leather recipe.

 Gather all equipment and measure all ingredients.

 In the biopot add all ingredients in the cold water and mix thoroughly.

 Low heat ingredients on hotplate and use a hand blender to mix them all.

 Stir until ingredients are combined and boiling.

 Take off the heat and carefully poor the bioplastic mixture over the baking tray or the silicone mat with the silicone strips.

 Place in oven at 30C for 48 hours.

 The bioplastic is going to warp and be a bit brittle. Rehydrate it using the water/glycerol mixture and massage it into the bioplastic. Once fully rehydrated you can place it in between two grills to keep it flat.




 Tips



 Gallery

No images found.

 More Ideas

---
category: bioplastics
title: Orange Bioplastic
author: Wendy
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,glycerol,vinegar,Cornflour,tapioca,agar,orange_and_lemon_peels
equipment: Bio Pot,Measuring Cup,1000g Scales,Silicone Spatula,Silicone Mat,Silicone Frames,heat_plate,spoon,grinder_or_blender
photo: recipes/opaquebioplastic-bag.jpeg
---




 Quantities Required


 500 ml cold water

 3 ml Glycerol

 9 ml Vinegar

 10g Corn flour

 10g Tapioca

 9g Agar

 Peels of 1 orange

 Peels of 1/2 lemon




 Steps


 Gather, prepare and measure all ingredients.

 Grind orange and lemon peels with 150 ml cold water.

 Add the rest of the ingredients in 350 ml cold water in bio pot and mix thoroughly.

 Add citrus peels to bio pot and bring to the boil.

 Remove and quickly pour in silicone mat or mold before it cools down and settles in bio pot.

 Transfer silicone mat or mold to the oven and let the bioplastic dry for 48 hours at 30C.




 Tips



To obtain a more flexible, see-through bioplastic, blend orange peels, boil them during 20 minutes and strained them before prior to the aforementioned process.



 Gallery

No images found.

 More Ideas

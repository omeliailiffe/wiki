---
category: bioplastics
title: Kelp Skin Bioplastic No. 3 (Foam)
author: Jaqueline Solis
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,Glycerol,Agar,White Vinegar,Tapioca Flour,Cornflour,seaweed_skin,seaweed_membrane,seaweed_flesh
equipment: Bio Pot,Teaspoon,Oven,Measuring Cup,baking_tray,Silicone Mat,Silicone Frames,Silicone Spatula,1000g Scales,Hotplate,hand_blender
photo: recipes/kelpskin3_1.jpeg
---




 Quantities Required


 500 ml Water

 20 ml Glycerol

 10 ml Vinegar

 10 gm Agar

 10 gm Tapioca

 10 gm Cornflour

 150 gm Seaweed Skin

 1.5 gm Seaweed Membrane

 29.5 gm Seaweed Flesh

 5 ml Liquid soap




 Steps



* For this recipe you will need the 1/2 mixture of the Kelp Skin Bioplastic no. 2


 Add liquid soap and whisk until foamy.

 Take off the heat and carefully poor 1/2 of the bioplastic mixture over the baking tray or the silicone mat with the silicone strips.

 Place in oven at 30C for 48 hours.




 Tips



 Gallery

No images found.

 More Ideas

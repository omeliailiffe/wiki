---
category: bioplastics
title: Opaque Bioplastic
author: Wendy
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: Glycerol,Agar,Cornflour,Tapioca Flour,White Vinegar
equipment: Bio Pot,Teaspoon,Oven,Beaker,Measuring Cup,Silicone Frames,Silicone Mat,1000g Scales,Hotplate,silicone_spatula
photo: recipes/opaquebioplastic-bag.jpeg
---




 Quantities Required


 Water (cold) - 500 ml

 Glycerol - 3 ml

 Agar - 9 g

 Cornflour - 10 g

 Tapioca - 10 g

 Vinegar - 9 g




 Steps


 Gather all equipment and measure all ingredients

 Set up the silicone mat and silicone frame to cover an A3 oven tray

 In the bio pot add all ingredients in the cold water and mix thoroughly

 Heat ingredients on hotplate, stirring continuously until boiling and all ingredients are combined

 Take off the heat and carefully poor the bioplastic mixture over the silicone mat, ensuring it is evenly distributed and fills all the corners of the silicone frame

 Place in oven at 25 degrees for 48 Hours




 Tips



Make sure you distribute agar evenly to ensure its mixed evenly into the liquid Bioplastic mixture.



If you don&#039;t poor the liquid bioplastic mixture quickly into the silicone frame it will begin to set in the bio pot.



 Gallery

No images found.

 More Ideas



The BioBag template is from Clara Davis, you can download it on https://www.thingiverse.com/thing:2548739

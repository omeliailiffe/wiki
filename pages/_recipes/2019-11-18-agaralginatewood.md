---
category: bioplastics
title: "Wood Bioplastic #1"
author: Wendy Neale
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 24 hour(s)
ingredients: Agar,Glycerol,Wood dust,Alginate,water
equipment: Bio Pot,Hotplate,Silicone Spatula,Mould
photo: recipes/agaralginatewood.jpeg
---




 Quantities Required



15g agar
5g glycerol
5g alginate
1/2 cup wood dust
250ml water



 Steps



Basic preparatory instructions go here.


 prepare a mould to pour your plastic into

 mix the alginate into the water and leave overnight

 add the agar and heat, while stirring, until the agar is properly dissolved and the solution thickens

 add the glycerol, stir

 add the wood dust and stir

 pour into the mould




 Tips



This one shrinks a lot. You will see in the photo below how much it shrunk.



 Gallery



 More Ideas

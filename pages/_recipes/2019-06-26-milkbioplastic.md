---
category: bioplastics
title: Milk Bioplastic
author: Maddie and Mathilde
difficulty: Moderate
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: White Vinegar,Milk
equipment: Bio Pot,Measuring Cup,measuring_spoon,Teaspoon,Hotplate,mould,Thermometer
---




 Quantities Required


 Milk - 120ml

 White Vinegar - 2tbsp




 Steps


 Gather all equipment and ingredients

 Measure milk, add to the bio pot

 Heat on hotplate until it reaches 45 degrees (measure this with a thermometer

 Add vinegar, stir until the mixture begins to seperate

 Strain over sieve or cotton cloth to get rid of any excess water/moisture

 Press into mould and leave for 48+ hours until set




 Tips


 Changing the type of milk will drastically change the outcome of your milk bioplastic (trim, whole, expired).

 Trim milk will mould together easier and become a very rigid, strong material that becomes slightly opaque when set.

 Whole milk will create a slightly crumbly mixture and will not dry opaque.

 By changing the measurements of vinegar you&#039;re adjusting the bioplastics consistency both when it&#039;s a liquid and when it&#039;s set.




 Gallery

No images found.

 More Ideas

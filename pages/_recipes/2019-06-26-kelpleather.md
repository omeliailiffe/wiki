---
category: bioplastics
title: Kelp Leather
author: Wendy Neale
difficulty: Moderate
prep-time: 3 hour(s)
curing-time: 24 hour(s)
ingredients: Bull Kelp,Glycerol
equipment: Hotplate,Oven
photo: recipes/kelp-leather.jpeg
---




Collect fresh bull kelp from a South coast beach. Make sure that it&#039;s not a reserve, as you&#039;re not allowed to take seaweed from reserves.


 Cut the bull kelp into a useful size for the pot.

 Roll it up, cover it with water and boil until the surface cells slough off.

 Leave to cool and scrape the rest of the surface cells off with a blunt scraper.

 Flay the bull kelp in half, exposing the internal honeycomb structure.

 Dry it in the oven @ 30°C overnight

 Soak in a 20% glycerol &amp; 80% water solution (ideally small so it all soaks up)

 Dry in oven overnight again.




 Tips



 More Ideas

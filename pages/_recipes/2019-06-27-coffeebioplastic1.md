---
category: bioplastics
title: Coffee bioplastic #1
author: Maddie and Mathilde
difficulty: Moderate
prep-time: 0.5 hour(s)
curing-time: 72 hour(s)
ingredients: water,Glycerol,Agar,Cornflour,Tapioca Flour,White Vinegar,tangerine,coffee,a4_sheet
equipment: 1000g Scales,Graduated Cylinder,Hotplate,Bio Pot,Oven,Silicone Frames,Silicone Mat,Measuring Cup,silicone_spatula
photo: recipes/coffeebioplastic1_1.jpeg
---




 Quantities Required


 Water - 500ml

 Glycerol - 10g

 Agar - 9g

 Cornflour - 10g

 Tapioca - 10g

 Vinegar - 9g

 Tangerine peels - 4g

 Ground coffee - 15g

 A4 sheet of Paper (preferably a used one)




 Steps


 Gather all equipment and ingredients

 Set up the silicone mat and frame to cover an A3 oven tray

 Measure all ingredients and add them to the bio pot

 Heat ingredients on hotplate, stirring continuously until boiling and all ingredients are combined

 Add the A4 sheet in small pieces

 Take off the heat and carefully poor the bioplastic mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame

 Place in oven at 25 degrees for 3 days




 Tips



By adding more paper you can create an interesting contrast with the coffee, however it also makes the bioplastic more rigid and firm.



You can put an oven tray on top of the coffee bioplastic to prevent it from changing shape in the oven.



 Gallery



 More Ideas

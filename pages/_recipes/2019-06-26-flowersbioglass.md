---
category: bioplastics
title: Brittle Bioglass
author: Maddie and Mathilde
difficulty: Moderate
prep-time: 0.5 hour(s)
curing-time: 72 hour(s)
ingredients: water,Agar,Glycerol,flowers
equipment: Hotplate,silicone_spatula,Silicone Frames,Silicone Mat,Bio Pot,Teaspoon,Measuring Cup,1000g Scales,Beaker,Oven
photo: recipes/brittlebioglass.jpeg
---




 Quantities Required


 Water - 480ml

 Agar - 36g

 Glycerol - 3g

 Flower Petals - As many as you desire




 Steps


 Gather all equipment and ingredients

 Set up the silicone mat and frame to cover an A3 oven tray

 Measure all ingredients and add them to the bio pot

 Heat ingredients on hotplate, stirring continuously until boiling and all ingredients are combined

 Take off the heat and carefully poor the bioglass mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame

 Add the flower petals above the preparation

 Place in oven at 25 degrees for 72 Hours




 Tips



 Gallery



 More Ideas

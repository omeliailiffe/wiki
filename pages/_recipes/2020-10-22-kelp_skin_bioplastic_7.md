---
category: bioplastics
title: Kelp Skin Bioplastic No. 7 (Cardboard-like)- From FRESH Kelp
author: Jacqueline Solís
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 24 hour(s)
ingredients: water,Glycerol,Agar,Alginate,seaweed_skin,Wood dust
equipment: 1000g Scales,measuring_cups,measuring_spoon,Silicone Spatula,Silicone Mat,biopot,Hotplate,hand_blender
photo: recipes/kelpskin6_1.jpeg
---




 Quantities Required


 250 ml Water

 5 ml Glycerol

 15 gm Agar

 5 gm Alginate

 120 ml seaweed skin from FRESH Kelp

 100 gm sawdust




 Steps



For this recipe you will need the skin you scrapped off the Kelp from the Kelp Leather recipe.


 Premoisture the wood dust.

 Mix all ingredients EXCEPT the wood dust in the biopot.

 Using a hand blender mix them all until all lumps are fully dissolved.

 Place the biopot in the hotplate and mix stirring constantly until it reaches 90C.

 While stirring add the wood dust slowly and mix thoroughly  

 Remove from heat and quickly pour into silicone mat.




 Tips



Out of he oven will be stiff. If you need it flexible you will have to hydrate it with a mixture of 80% water and 20% glycerol by massaging it slightly into the material. For this sample 5 ml total was used.



 Gallery

No images found.

 More Ideas

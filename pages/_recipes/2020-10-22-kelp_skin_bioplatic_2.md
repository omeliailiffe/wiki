---
category: bioplastics
title: Kelp Skin Bioplastic No. 2 (Flexible)
author: Jaqueline Solis
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,Glycerol,White Vinegar,Agar,Tapioca Flour,Cornflour,seaweed_skin,seaweed_membrane,seaweed_flesh
equipment: Bio Pot,Teaspoon,Oven,Measuring Cup,baking_tray,Silicone Spatula,Hotplate,hand_blender,1000g Scales
photo: recipes/kelpskin2_1.jpeg
---




 Quantities Required


 500 ml Water

 20 ml Glycerol

 10 ml Vinegar

 10 gm Agar

 10 gm Tapioca

 10 gm Cornflour

 150 gm Seaweed Skin (what you scrapped off from the Leather recipe)

 1.5 gm Seaweed Membrane (Kelp “honeycomb” structure)

 29.5 gm Seaweed Flesh (Kelp “body”)




 Steps


 For this recipe you will need the skin you scrapped off from the Kelp leather recipe.

 Gather all equipment and measure all ingredients.

 In the biopot add all ingredients in the cold water and mix thoroughly.

 Low heat ingredients on hotplate and use a hand blender to mix them all.

 Stir until ingredients are combined and boiling.

 Take off the heat and carefully poor 1/2 of the bioplastic mixture over the baking tray or the silicone mat with the silicone strips.

 Place in oven at 30C for 48 hours.




Note: The other 1/2 of the mixture will be used to make the Kelp Skin Bioplastic No. 3 (Foam).



 Tips



 Gallery

No images found.

 More Ideas

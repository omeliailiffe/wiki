---
category: bioplastics
title: Kelp Skin Bioplastic No. 4 (Flexible) - From FRESH Kelp
author: Jaqueline Solis
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,Glycerol,White Vinegar,Agar,Tapioca Flour,Cornflour,seaweed_sking_from_fresh_kelp
equipment: 1000g Scales,Teaspoon,biopot,Silicone Spatula,hand_blender,oven,Silicone Mat,Measuring Cup
photo: recipes/kelpskin4_1.jpeg
---




 Quantities Required


 500 ml Water

 20 ml Glycerol

 10 ml Vinegar

 10 gm Agar

 10 gm Tapioca

 10 gm Cornflour

 120 gm Seaweed Skin from Fresh Kelp




 Steps


 For this recipe you will need the skin you scrapped off from the Kelp leather recipe.

 Gather all equipment and measure all ingredients.

 In the biopot add all ingredients in the cold water and mix thoroughly.

 Low heat ingredients on hotplate and use a hand blender to mix them all.

 Stir until ingredients are combined and boiling.

 Take off the heat and carefully poor the bioplastic mixture over the baking tray or the silicone mat with the silicone strips.

 Place in oven at 30C for 48 hours.




 Tips



 Gallery

No images found.

 More Ideas

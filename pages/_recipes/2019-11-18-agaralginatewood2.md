---
category: bioplastics
title: "Wood Bioplastic #2B"
author: Wendy Neale
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 24 hour(s)
ingredients: Agar,Glycerol,Carnauba Wax,Wood dust,Alginate,water
equipment: Bio Pot,Silicone Spatula,1000g Scales,Mould
photo: recipes/agaralginatewood2.jpeg
---




 Quantities Required



15g agar, 5g glycerol, 5g alginate, 5g carnauba wax, 10g beeswax, 3 cups wood dust, 250ml water



 Steps



the day before you wish to make this:


 prepare a mould to pour your plastic into

 mix the alginate into the water and leave overnight

 soak the wood dust in an amount of water to over the dust




the next day:


 add the agar to the alginate mixture and heat, while stirring, until the agar is properly dissolved

 add the waxes, stir until the waxes melt and the solution thickens the solution thickens

 add the glycerol, stir

 squeeze excess water out of wood dust as you gradually add and stir

 pour into the mould

 put the mould in the oven at 50°C for a few hours, then turn it down to 40°C overnight




 Tips



 Gallery

No images found.

 More Ideas

---
category: bioplastics
title: Kelp Skin Bioplastic No. 6 (Semi-Rigid) - From FRESH Kelp
author: Jaqueline Solis
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,Glycerol,White Vinegar,Agar,Cornflour,Tapioca Flour,seaweed_skin
equipment: 1000g Scales,Measuring Cup,Teaspoon,Silicone Spatula,biopot,hand_blender,Silicone Mat,Hotplate,Oven
photo: recipes/kelpskin6_1.jpeg
---




 Quantities Required

* Water 500 ml
* Glycerol 6 ml
* Vinegar 9 ml
* Agar 10 gm
* Corn Flour 10 gm
* Tapioca Flour 10 gm
* Seaweed Skin 120 gm


 Steps



This recipe uses the seaweed skin that you scrapped off the Kelp Leather recipe using the FRESH Kelp.


 Gather all equipment and measure all ingredients.

 In the bio pot add all ingredients in the cold water and mix thoroughly.

 Low heat ingredients on hotplate and use a hand blender to mix them all, then stir until all ingredients are combined and boiling.

 Take off the heat and carefully poor the bioplastic mixture over a fenced baking tray ensuring it is evenly distributed.

 Place in oven at 30 degrees Celsius for 48 hours.  




 Tips



 Gallery

No images found.

 More Ideas

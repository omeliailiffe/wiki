---
category: bioplastics
title: Clear Agar Bioplastic
author: Green Plastics, by E.S. Stevens
difficulty: Easy
prep-time: 0.5
curing-time: 48
ingredients: Glycerol, Agar, water
equipment: Bio Pot, Teaspoon, Oven, Beaker, Measuring Cup, Silicone Mat, Silicone Frames, 1000g Scales, Hotplate
photo: recipes/ClearBioplastic.jpeg
---

## Instructions
### Quantities Required

* Water - 420 mL
* Glycerol - 2.5 mL
* Agar - 4 g


### Steps

1. Gather all equipment and ingredients
2. Set up the silicone mat and frame to cover an A3 oven tray
3. Measure all ingredients and add them to the bio pot
4. Heat ingredients on hotplate, stirring continuously until homogenous and bubbles have begun forming (around 90-95 C)
5. Assure there is no sediment or undissolved material in the pot. Skim any bubbles on the surface.
6. Take off the heat and carefully poor the bioplastic mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame
7. Place in oven at 30 degrees for 48 Hours

### Tips

This recipe will create a bioplastic that is clear and cellophane-like in texture. It has a tendency to stick to itself if still drying.

You can also flip the bioplastic halfway through its curing time to allow for the extra moisture on the bottom side to evaporate, however this may result in cloudy patches. The material will lift slightly from the tray as it dries at which point is is easy to pick up.


### Photos

![Flat lay]({{site.imageurl}}recipes/FlatLayClear1.jpeg){:.img-fluid}
![Flat lay]({{site.imageurl}}recipes/FlatLayClear2.jpeg){:.img-fluid}
![Transparency]({{site.imageurl}}recipes/ClearBioplastic.jpeg){:.img-fluid}

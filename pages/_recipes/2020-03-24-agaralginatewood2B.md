---
category: bioplastics
title: "Wood Bioplastic #2A"
author: Wendy Neale
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,wood_dust,carnauba_wax,alginate,agar,Glycerol
equipment: bio_pot,Silicone Spatula,heat_plate,Measuring Cup,1000g Scales,Teaspoon,blender,mold
---




 Quantities Required


 250 ml Cold water

 2 cups Wood dust

 2g Carnauba wax

 5g Alginate

 15g Agar

 5ml Glycerol




 Steps


 PRE-STEP: Mix thoroughly the alginate and let is soak in the cold water overnight. If necessary, you can use a hand blender.

 Gather tools and measure all ingredients.

 Use the same bio pot where you prepared the orange bioplastic without rinsing it.

 In the bio pot add the soaked alginate and warm up in heat plate.

 Add the agar and the glycerol and mix thoroughly.

 Add the wax to mix until completely melted.

 Add the wood dust and stir, remove from heat and pour into the mold.

 Let to dry in over at 30C for 48 hours.




 Tips



This recipe turned out a bit densed, and it was necessary to spray water on top of the mixture post pouring on mold.



 Gallery



 More Ideas

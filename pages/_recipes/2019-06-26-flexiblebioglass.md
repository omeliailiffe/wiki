---
category: bioplastics
title: Flexible Bioglass
author: Maddie and Mathilde
difficulty: Easy
prep-time: 0.3 hour(s)
curing-time: 72 hour(s)
ingredients: Agar,Glycerol
equipment: Bio Pot,Oven,Hotplate,Teaspoon,Beaker,Measuring Cup,1000g Scales,Silicone Frames,Silicone Mat
---




 Quantities Required


 Water - 480ml

 Agar - 36g

 Glycerol - 5g




 Steps



Basic preparatory instructions go here.


 Gather all equipment and ingredients

 Set up the silicone mat and frame to cover an A3 oven tray

 Measure all ingredients and add them to the bio pot

 Heat ingredients on hotplate, stirring continuously until boiling and all ingredients are combined

 Take off the heat and carefully poor the bioglass mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame

 Place in oven at 25 degrees for 72 Hours




 Tips



To create a more brittle, glass-like bioglass you can play around with the measurements of glycerol. (Glycerol is a plasticiser which changes the flexibility of the material.)



3g of glycerol will create both a lighter, thinner and far more brittle version of the Flexible Bioglass.



 Gallery

No images found.

 More Ideas

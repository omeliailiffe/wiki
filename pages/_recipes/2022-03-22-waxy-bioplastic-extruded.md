---
category: bioplastics
title: Waxy Bioplastic (Extruded)
author: Eva
difficulty: Moderate
prep-time: 0.75
curing-time: Variant
ingredients: Glycerol, Agar, Water, Wax, Pulp
equipment: Bio Pot, Teaspoon, Oven, Beaker, Measuring Cup, Silicone Mat, Silicone Frames, 1000g Scales, Hotplate, Immersion Blender, Paper Scraps or Paper Pulp, Large Syringe
photo: recipes/ExtrudedBioPlastic.jpeg
---

## Instructions
### Quantities Required

* Water - 50 mL
* Glycerol - 10 g
* Agar - 5 g
* Wet Paper Pulp - 100g
* Wax - 5 g

### Steps

1. Gather all equipment and ingredients
2. Set up the silicone mat or mould
3. Stirring constantly, heat ingredients on medium until wax is melted and consistency is thick
4. Using the immersion blender, blend until you can no longer see any wax on the surface of the mixture
5. Using the syringe, pipe the mixture onto your silicone mat (or pour into your mould)
6. If the mixture gets stuck in the syringe, continue to blend until a smooth paste consistency is reached and try extruding again
7. Cure for 24 - 72 Hours at room temperature, your curing time is dependent on the thickness of your samples

### Tips

This recipe will create a bioplastic that is stretchy and thick that can hold a shape.
You can dry in the oven as well with little change in the final outcome.
It would be interesting to try drying this in a mould to see how much it shrinks from the walls of the mould
At the bottom of the photo is a sample which I extruded onto some of the PAPER PULP BIOPLASTIC


### Photos

![Extruded Samples]({{site.imageurl}}recipes/ExtrudedBioPlastic.jpeg){:.img-fluid}

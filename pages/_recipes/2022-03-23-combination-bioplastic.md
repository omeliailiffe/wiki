---
category: bioplastics
title: Combination Bioplastic
author: Eva
difficulty: Moderate
prep-time: 0.75 - 1
curing-time: 48
ingredients: Glycerol, Agar, water
equipment: Bio Pot, Teaspoon, Oven, Beaker, Measuring Cup, Silicone Mat, Silicone Frames, 1000g Scales, Hotplate, Immersion Blender, Paper Scraps or Paper Pulp, Large Syringe
photo: recipes/FlatLayGrid.jpeg
---

## Instructions
### Quantities Required

#### Base
* Water - 420 mL
* Glycerol - 2.5 mL
* Agar - 4 g

#### Grid
* Water - 420 mL
* Glycerol - 2.5 mL
* Agar - 4 g
* Paper Pulp - 10g (dry)

### Steps

1. Gather all equipment and ingredients
2. Set up the silicone mat and frame to cover an A3 oven tray
3. Follow Recipe for CLEAR BIOPLASTIC and pour onto your prepared oven tray
4. Follow Recipe for PAPER PULP BIOPLASTIC
5. Instead of pouring the pulp bioplastic onto the oven tray, carefully syringe up the mixture and pipe it onto the clear bioplastic
6. You can create any pattern you'd like, I made a simple grid
7. Cure for 48 - 72 Hours at room temperature

### Tips

This recipe will create a bioplastic that is a combination of the clear and paper pulp bioplastics.
The better ventilated the space the faster this material will dry.
I personally recommend not touching this bioplastic once you have poured it to get a flat sheet.
It would be interesting to try drying this in the oven as the paper pulp bioplastic shrinks more than the clear bioplastic, this could create a bouclé-like effect


### Photos

![Flat Lay]({{site.imageurl}}recipes/FlatLayGrid.jpeg){:.img-fluid}
![Touch]({{site.imageurl}}recipes/GridBioplastic.jpeg){:.img-fluid}
![Transparency]({{site.imageurl}}recipes/TransparencyTestGrid.jpeg){:.img-fluid}
![Close Up 1]({{site.imageurl}}recipes/GridCloseUp1.jpeg){:.img-fluid}
![Close Up 2]({{site.imageurl}}recipes/GridCloseUp2.jpeg){:.img-fluid}

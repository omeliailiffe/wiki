---
category: bioplastics
title: Spirulina Agar Bioplastic
author: Jaqueline Solis
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: Agar,Glycerol,spirulina,water,beeswax
equipment: Bio Pot,Silicone Spatula,Teaspoon,Silicone Mat
photo: recipes/spirulina-plastic.jpeg
---




 Quantities Required



- 500ml water



- 25gm agar



- 30ml glycerol



- 2.5gm spirulina powder



- 2 beeswax pellets (for texture)



 Steps



* Mix agar and spirulina together in the cold water.



* Heat up mixture slowly and add wax pellets.



* When temperature reaches 47 degrees add glycerol.



* Stir until it reaches 95 degrees and then remove from heat.



* Pour it onto the silicone mat according to the shape you wish.



* Dry in the oven at 26 - 30 degrees for 2 to 3 days



 Tips



You can make the colour darker by adding more spirulina. If you add the spirulina after the agar, it won&#039;t dissolve, but will be suspended, creating a different effect.



 Gallery

No images found.

 More Ideas

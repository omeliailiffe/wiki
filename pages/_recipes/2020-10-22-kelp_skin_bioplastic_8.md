---
category: bioplastics
title: Kelp Skin Bioplastic No. 8 (Semi-Rigid &amp; Dyed)
author: Jacqueline Solís
difficulty: Easy
prep-time: 0.5 hour(s)
curing-time: 48 hour(s)
ingredients: water,Glycerol,White Vinegar,Agar,Tapioca Flour,Cornflour,seaweed_skin,spirulina_food_colourant
equipment: 1000g Scales,Teaspoon,biopot,Hotplate,Silicone Spatula,hand_blender,Oven
photo: recipes/kelpskin8_1.jpeg
---




 Quantities Required


 Water 500 ml

 Glycerol 6 ml

 Vinegar 9 ml

 Agar 9 gm

 Corn Flour 10 gm

 Tapioca Flour 10 gm

 Seaweed Skin 115 gm

 Spirulina food colouring 5 to 10 drops, depending on desired shade




 Steps



This recipe uses the seaweed skin that you scrapped off the Kelp Leather recipe.


 Gather all equipment and measure all ingredients.

 In the bio pot add all ingredients in the cold water and mix thoroughly.

 Low heat ingredients on hotplate and use a hand blender to mix them all, then stir until all ingredients are combined and boiling.

 Take off the heat and carefully poor half of the bioplastic mixture over one half of TWO fenced baking trays ensuring it is evenly distributed.

 Very quickly, add 5 drops of the Spirulina food colorant in the other half of the bioplastic mixture and mix well, then quickly poor it over the other half of the TWO fenced baking trays ensuring it is evenly distributed.  

 Place in oven at 30 degrees Celsius for 48 hours.




 Tips



 Gallery

No images found.

 More Ideas

---
category: bioplastics
title: Coffee bioplastic #2
author: Maddie and Mathilde
difficulty: Moderate
prep-time: 0.5 hour(s)
curing-time: 72 hour(s)
ingredients: water,Agar,Glycerol,Cornflour,Tapioca Flour,vine,coffee,leaves
equipment: 1000g Scales,Hotplate,Bio Pot,Measuring Cup,Graduated Cylinder,Silicone Frames,Silicone Mat,oven,silicone_spatula
photo: recipes/coffeebioplastic2_1.jpeg
---




 Quantities Required


 Water - 500ml

 Glycerol - 10g

 Agar - 9g

 Cornflour - 10g

 Tapioca - 10g

 Vinegar - 9g

 Ground coffee - 30g

 Small leaves




 Steps


 Gather all equipment and ingredients

 Set up the silicone mat and frame to cover an A3 oven tray

 Measure all ingredients and add them to the bio pot

 Heat ingredients on hotplate, stirring continuously until boiling and all ingredients are combined

 Add the small leaves

 Take off the heat and carefully poor the bioplastic mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame

 Place in oven at 25 degrees for 3 days




 Tips



You can put an oven tray on top of the coffee bioplastic to prevent it from changing shape in the oven.



 Gallery



 More Ideas

---
category: bioplastics
title: Paper Pulp Bioplastic
author: Eva
difficulty: Moderate
prep-time: 0.5 - 1
curing-time: 48
ingredients: Glycerol, Agar, water
equipment: Bio Pot, Teaspoon, Oven, Beaker, Measuring Cup, Silicone Mat, Silicone Frames, 1000g Scales, Hotplate, Immersion Blender, Paper Scraps or Paper Pulp
photo: recipes/AirDryPulpPlastic.jpeg
---

## Instructions
### Quantities Required

* Water - 420 mL
* Glycerol - 2.5 mL
* Agar - 4 g
* Paper Pulp - 10g (dry)


### Steps

1. Gather all equipment and ingredients
2. Set up the silicone mat and frame to cover an A3 oven tray
3. Soak your paper pulp (or scrap paper pieces) in the water until they are saturated
4. Using the Immersion blender, blend together the pulp and water to your desired consistency and all of the pulp is suspended in the water
5. Add all ingredients to pot and stir to combine
6. Heat ingredients on hotplate, stirring continuously until homogenous and bubbles have begun forming (around 90-95 C)
7. Assure there is no sediment or undissolved material in the pot, the mixture should be bubbling slightly and have a thick texture (at this point you can CAREFULLY blend the mixture again to assure everything is fully incorporated)
8. Take off the heat and carefully poor the bioplastic mixture over the silicone matt, ensuring it is evenly distributed and fills all the corners of the silicone frame
9. Place in oven at 30 degrees for 48 Hours (Image 1) OR Place at room temperature for 48 - 72 Hours (Image 2)

### Tips

This recipe will create a bioplastic that is cloudy and flexible.
The better ventilated the space the faster this material will dry.
I personally recommend not touching this bioplastic once you have poured it, sample 1 was flipped halfway through to dry quicker which resulted in the edges curling quite dramatically. Both Sample 1 and Sample 2 were poured into the same size tray but the size varies quite dramatically between them.
The same amount and source of paper pulp was used in both recipes, but Sample 1 is considerably darker. I believe this is due to the drying process.


### Photos

![Sample 1 - Oven Dried]({{site.imageurl}}recipes/OvenDryPulpPlastic.jpeg){:.img-fluid}
![Sample 2 - Air Dried]({{site.imageurl}}recipes/AirDryPulpPlastic.jpeg){:.img-fluid}
